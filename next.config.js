/* eslint-disable no-undef */
require('dotenv').config()

module.exports = {
    poweredByHeader: false,
    compress: false,
    target: 'serverless',
    env: {
        APP_NAME: process.env.APP_NAME,
    },
}