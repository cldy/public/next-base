import React from 'react'
import Head from 'next/head'
import { Box, BoxProps } from '@cloudey/storm'

export const Page = ({children, title, ...props}: BoxProps & { title: string }) => {
    const pageTitle = `${title} | ${process.env.APP_NAME}`

    return (
        <>
            {title &&
                <Head>
                    <title>{pageTitle}</title>
                    <meta property={'og:title'} content={pageTitle} key={'title'}/>
                </Head>
            }
            <Box {...props}>
                {children}
            </Box>
        </>
    )
}