declare global {
    namespace NodeJS {
        interface ProcessEnv {
            APP_NAME: string;
            NODE_ENV: 'development' | 'production';
            PORT?: string;
            PWD: string;
        }
    }
}

export {}