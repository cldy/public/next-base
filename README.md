# Next-Base

**Flexible boilerplate for Next.js based applications.**  
Used for Cloudey projects.

[Cloudey](https://cloudey.net)

Licensed under MIT.