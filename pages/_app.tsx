import { AppProps } from 'next/app'
import * as React from 'react'
import { ThemeProvider } from 'emotion-theming'
import { global, theme } from '@cloudey/storm'
import { Global } from '@emotion/core'
import Head  from 'next/head'
import { nunitoSans } from '../fonts/nunitoSans'

export default function App ({Component, pageProps}: AppProps) {
    const title = process.env.APP_NAME

    return (
        <ThemeProvider theme={theme}>
            <Global styles={{
                ...global,
                ...nunitoSans,
            }} />
            <Head>
                <title>{title}</title>
                <meta property={'og:title'} content={title} key={'title'} />
            </Head>
            <Component {...pageProps}/>
        </ThemeProvider>
    )
}
