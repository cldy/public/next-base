import React from 'react'
import { Page } from '@/Page'

export default function Index () {
    return (
        <Page title={'Index'} fontFamily={'nunitoSans'} fontWeight={'300'}>
            Hello.
        </Page>
    )
}
