export const nunitoSans = [
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 200,
            src: `local("Nunito Sans ExtraLight"), url("/fonts/nunito-sans/nunito-sans-200.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-200.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 200,
            src: `local("Nunito Sans ExtraLight Italic"), url("/fonts/nunito-sans/nunito-sans-200italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-200italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 300,
            src: `local("Nunito Sans Light"), url("/fonts/nunito-sans/nunito-sans-300.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-300.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 300,
            src: `local("Nunito Sans Light Italic"), url("/fonts/nunito-sans/nunito-sans-300italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-300italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 400,
            src: `local("Nunito Sans"), url("/fonts/nunito-sans/nunito-sans-regular.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-regular.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 400,
            src: `local("Nunito Sans Italic"), url("/fonts/nunito-sans/nunito-sans-italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 600,
            src: `local("Nunito Sans SemiBold"), url("/fonts/nunito-sans/nunito-sans-600.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-600.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 600,
            src: `local("Nunito Sans SemiBold Italic"), url("/fonts/nunito-sans/nunito-sans-600italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-600italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 700,
            src: `local("Nunito Sans Bold"), url("/fonts/nunito-sans/nunito-sans-700.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-700.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 700,
            src: `local("Nunito Sans Bold Italic"), url("/fonts/nunito-sans/nunito-sans-700italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-700italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 800,
            src: `local("Nunito Sans ExtraBold"), url("/fonts/nunito-sans/nunito-sans-800.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-800.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 800,
            src: `local("Nunito Sans ExtraBold Italic"), url("/fonts/nunito-sans/nunito-sans-800italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-800italic.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'normal',
            fontWeight: 900,
            src: `local("Nunito Sans Black"), url("/fonts/nunito-sans/nunito-sans-900.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-900.woff") format("woff")`,
        }
    },
    {
        '@font-face': {
            fontFamily: '"Nunito Sans"',
            fontStyle: 'italic',
            fontWeight: 900,
            src: `local("Nunito Sans Black Italic"), url("/fonts/nunito-sans/nunito-sans-900italic.woff2") format("woff2"), url("/fonts/nunito-sans/nunito-sans-900italic.woff") format("woff")`,
        }
    },
]
